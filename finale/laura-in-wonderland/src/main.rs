use std::{collections::HashMap, fs};

fn main() {
    let replacement_map: HashMap<String, String> = HashMap::from([
        ("Alice's".to_string(), "Laura's".to_string()),
        ("Alice!".to_string(), "Laura!".to_string()),
        ("Alice,".to_string(), "Laura,".to_string()),
        ("Alice;".to_string(), "Laura;".to_string()),
        ("(Alice".to_string(), "(Laura".to_string()),
        ("Alice,)".to_string(), "Laura,)".to_string()),
        ("Alice".to_string(), "Laura".to_string()),
        ("Alice’s".to_string(), "Laura’s".to_string()),
    ]);

    let words = get_content("alice_chapter_1").unwrap();
    let updated_vec_of_strings = replace_alice_in_place(words.clone(), &replacement_map).unwrap();
    let updated_text: String =
        compile_updated_text(&build_updated_lines(&updated_vec_of_strings)).unwrap();
    fs::write("laura_chapter_1", updated_text).unwrap();
}

fn get_content(filepath: &str) -> Result<Vec<Vec<String>>, std::io::Error> {
    let result: Vec<Vec<String>> = fs::read_to_string(filepath)?
        .lines()
        .map(|line| {
            line.split_whitespace()
                .map(|word| word.to_string())
                .collect()
        })
        .collect();

    Ok(result)
}

#[warn(dead_code)]
// this won't find occurences with ,;!) at the end!
fn replace_name(
    mut words: Vec<Vec<String>>,
    new_name: &str,
) -> Result<Vec<Vec<String>>, std::io::Error> {
    for v in &mut words {
        v.iter_mut()
            .filter(|name| name.to_string() == String::from("Alice"))
            .for_each(|name| *name = String::from(new_name))
    }
    Ok(words)
}

fn replace_alice_in_place(
    mut lines: Vec<Vec<String>>,
    replace_map: &HashMap<String, String>,
) -> Result<Vec<Vec<String>>, std::io::Error> {
    for line in lines.iter_mut() {
        line.iter_mut()
            .filter(|word| replace_map.contains_key(*word))
            .for_each(|word| *word = replace_map.get(word).unwrap().to_string())
    }
    Ok(lines)
}

fn build_updated_lines(lines: &Vec<Vec<String>>) -> Vec<String> {
    let result = lines.iter().map(|line| line.join(" ")).collect();

    result
}

fn compile_updated_text(updated_lines: &Vec<String>) -> Result<String, std::io::Error> {
    let result: String = updated_lines.join("\n");

    Ok(result)
}
