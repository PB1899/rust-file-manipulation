use std::fs;
fn main() {
    let filepath = "../file_with_lines.txt";
    println!("{:#?}", read_file(filepath));
}

fn read_file(filepath: &str) -> Result<Vec<String>, std::io::Error> {
    let content: String = fs::read_to_string(filepath).expect("can't parse file");
    let result = content
        .lines()
        .map(|l| l.to_string())
        .collect::<Vec<String>>();
    Ok(result)
}
