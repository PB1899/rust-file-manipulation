use std::fs;

fn main() {
    let file_path = "words_to_file";
    let words = vec![
        "Words".to_string(),
        "of".to_string(),
        "the".to_string(),
        "first".to_string(),
        "line".to_string(),
    ];

    // Implement the write_words_to_file function
    write_words_to_file(&file_path, &words).unwrap();
}

fn write_words_to_file(filepath: &str, words: &Vec<String>) -> Result<(), std::io::Error> {
    let text = words.join(" ");
    fs::write(filepath, text).expect("coldn't write file");
    Ok(())
}
