use std::fs;

fn main() {
    let file_path = "../file_with_lines.txt";

    // Implement the read_file function
    let lines = words_of_lines(&file_path).expect(&format!("Unable to read file <{}>", &file_path));

    println!("{:#?}", lines);
}

fn words_of_lines(filepath: &str) -> Result<Vec<Vec<String>>, std::io::Error> {
    let result: Vec<Vec<String>> = fs::read_to_string(filepath)?
        .lines()
        .map(|line| {
            line.to_string()
                .split_whitespace()
                .map(|word| word.to_string())
                .collect::<Vec<String>>()
        })
        .collect();

    Ok(result)
}
