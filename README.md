### Rust file manipulation

Exercises from the course [Rust file manipulation](https://www.linkedin.com/learning/practice-it-rust-file-manipulation/)

[Exercise files on GitHub](https://github.com/LinkedInLearning/practice-it-rust-file-manipulation-3082491)